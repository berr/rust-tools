use std::io::{self, Read};
use std::fs::File;
use rwc::Counter;
use std::process;

extern crate clap;

struct Parameters {
    pub source: String,
    pub lines: bool,
    pub words: bool,
    pub bytes: bool,
}

fn run(parameters: &Parameters) -> io::Result<()> {
    if parameters.source == "-" {
        let stdin = io::stdin();
        let mut handle = stdin.lock();
        return count_from_reader(&mut handle, parameters);
    } else {
        let mut f = File::open(&parameters.source)?;
        return count_from_reader(&mut f, parameters);
    }
}

fn count_from_reader(reader: &mut impl Read, parameters: &Parameters) -> io::Result<()> {
    let mut read_buffer = [0 as u8; 512];
    let mut counter = Counter::new();
    loop {
        let read_size = reader.read(&mut read_buffer)?;
        if read_size == 0 {
            break
        }

        counter.feed(&read_buffer[0..read_size]);
    }

    let stats = counter.stats();

    let mut outputs= vec![];

    if parameters.lines {
        outputs.push(stats.lines);
    }

    if parameters.words {
        outputs.push(stats.words);
    }

    if parameters.bytes {
        outputs.push(stats.bytes);
    }


    let s : Vec<String> = outputs.iter().map(|n| n.to_string()).collect();
    println!("{}", s.join(" "));

    return Ok(());
}

fn get_parameters() -> Parameters {
    let matches = clap::App::new("Rust Word Count")
                          .version("1.0")
                          .arg(clap::Arg::with_name("file")
                               .help("Read data from file. - or no parameter to read from stdin")
                               .short("f")
                               .long("file")
                               .takes_value(true)
                               .index(1))
                          .arg(clap::Arg::with_name("words")
                               .help("Count number of words")
                               .short("w")
                               .long("words"))
                          .arg(clap::Arg::with_name("lines")
                               .help("Count number of lines")
                               .short("l")
                               .long("lines"))
                          .arg(clap::Arg::with_name("bytes")
                               .help("Count number of bytes")
                               .short("b")
                               .long("bytes"))
                          .get_matches();



    let source = matches.value_of("file").unwrap_or("-").to_string();

    let print_lines = matches.is_present("lines");
    let print_words = matches.is_present("words");
    let print_bytes = matches.is_present("bytes");

    if print_lines || print_words || print_bytes {
        return Parameters {source, lines: print_lines, words: print_words, bytes: print_bytes}
    } else {
        return Parameters {source, lines: true, words: true, bytes: true}
    };
}

fn main() {
    let parameters = get_parameters();

    match run(&parameters) {
        Ok(()) => {},

        Err(e) => {
            eprintln!("rwc: {}: {}", parameters.source, e);
            process::exit(1);
        }
    }
}
