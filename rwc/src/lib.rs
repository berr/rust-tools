#[derive(Debug, PartialEq)]
pub struct CountResult {
    pub lines: i32,
    pub words: i32,
    pub bytes: i32,
}

pub struct Counter {
    lines: i32,
    words: i32,
    bytes: i32,
    previous_was_space: bool,
}

impl Counter {

    pub fn new() -> Counter {
        return Counter {lines: 0, words: 0, bytes: 0, previous_was_space: true};
    }

    pub fn feed_text(&mut self, text: &str) {
        self.feed(text.as_bytes());
    }

    pub fn feed(&mut self, input: &[u8]) {
        const SPACES : [u8; 3] = [b' ', b'\t', b'\n'];

        for c in input {
            self.bytes += 1;

            if *c == b'\n' {
                self.lines += 1;
            }

            let current_is_space = SPACES.iter().any(|s| s == c);
            if self.previous_was_space && !current_is_space {
                self.words += 1;
            }

            self.previous_was_space = current_is_space;
        }
    }

    pub fn stats(&self) -> CountResult {
        return CountResult { lines: self.lines, words: self.words, bytes: self.bytes };
    }
}

pub fn count(input: &str) -> CountResult {
    let mut counter = Counter::new();
    counter.feed_text(input);
    return counter.stats();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty_string_returns_empty_result() {
        let input =  "";
        let obtained = count(input);

        let expected = CountResult{ lines: 0, words: 0, bytes: 0};

        assert_eq!(obtained, expected);
    }

    #[test]
    fn read_number_of_lines() {
        let input =  "first\nsecond\nthird";
        let obtained = count(input);

        let expected = CountResult{ lines: 2, words: 3, bytes: 18};

        assert_eq!(obtained, expected);
    }

    #[test]
    fn empty_lines_are_not_ignored() {
        let input =  "\nsecond\n\n";
        let obtained = count(input);

        let expected = CountResult{ lines: 3, words: 1, bytes: 9};

        assert_eq!(obtained, expected);
    }

    #[test]
    fn count_words_separated_by_spaces() {
        let input =  "one two";
        let obtained = count(input);

        let expected = CountResult{ lines: 0, words: 2, bytes: 7};

        assert_eq!(obtained, expected);
    }

    #[test]
    fn tab_is_considered_space() {
        let input =  "one\ttwo\tthree";
        let obtained = count(input);

        let expected = CountResult{ lines: 0, words: 3, bytes: 13};

        assert_eq!(obtained, expected);
    }

    #[test]
    fn new_line_is_considered_whitespace() {
        let input =  "first\nsecond\nthird";
        let obtained = count(input);

        let expected = CountResult{ lines: 2, words: 3, bytes: 18};

        assert_eq!(obtained, expected);
    }

    #[test]
    fn multiple_whitespaces_are_ignored() {
        let input =  "first: \n\tsecond\n\nThird:\n\t\tFourth";
        let obtained = count(input);

        let expected = CountResult{ lines: 4, words: 4, bytes: 32};

        assert_eq!(obtained, expected);
    }

    #[test]
    fn number_of_bytes() {
        let input =  "1234\n6\t 9";
        let obtained = count(input);

        let expected = CountResult{ lines: 1, words: 3, bytes: 9};

        assert_eq!(obtained, expected);
    }

    #[test]
    fn utf8_characters_are_considered_as_bytes() {
        let input =  "That's a pile of \u{1F4A9}!";
        let obtained = count(input);

        let expected = CountResult{ lines: 0, words: 5, bytes: 22};

        assert_eq!(obtained, expected);
    }
}